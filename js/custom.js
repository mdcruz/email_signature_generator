////////////////////////
// FUNCTIONS
////////////////////////

//
// Place raw HTML of each version into appropriate containers
function updateHtmlSigRaw() {
    get_html_signature_full_visual = document.getElementById("signature-full-visual").innerHTML;

    $("#signature-full-html textarea").text(get_html_signature_full_visual);

}

////////////////////////
// INITIALIZE
////////////////////////

//
// Activate Bootstrap tabs
$("#html-signature-full-tabs a").click(function (e) {
    e.preventDefault();
    $(this).tab('show');
})
$("#html-signature-horizontal-tabs a").click(function (e) {
    e.preventDefault();
    $(this).tab('show');
})

//
// Set variables and defaults: Credit: simplified by Mat
var employee = {
    defaultProfile: "http://img.android-apk.org/imgs/1/4/7/147c5fabb25414aed1e51a87bda0b03f_icon_96x96.png",
    defaultName: "Melvin D. Cruz",
    defaultCompany: "StraightArrow Corporation",
    defaultPosition: "Web Specialist I",
    defaultEmail: "mdcruz@straightarrow.com.ph",
    defaultPhone: "555-5555-5555",
    defaultMobile: "555-5555-5555",
    profile: "",
    name: "",
    company: "",
    position: "",
    email: "",
    phone: "",
    mobile: "",
    setProfile: function(newProfile){
        this.profile = newProfile;
    },
    getProfile: function(){
        if(this.profile.length == 0){
            return this.defaultProfile;
        }
        return this.profile;
    },
    setName: function(newName){
        this.name = newName;
    },
    getName: function(){
        if(this.name.length == 0){
            return this.defaultName;
        }
        return this.name;
    },
    setCompany: function(newCompany){
        this.company = newCompany;
    },
    getCompany: function(){
        if(this.company.length == 0){
            return this.defaultCompany;
        }
        return this.company;
    },
    setPosition: function(newPosition){
        this.position = newPosition;
    },
    getPosition: function(){
        if(this.position.length == 0){
            return this.defaultPosition;
        }
        return this.position;
    },
    setEmail: function(newEmail){
        this.email = newEmail;
    },
    getEmail: function(){
        if(this.email.length == 0){
            return this.defaultEmail;
        }
        return this.email;
    },
    setPhone: function(newPhone){
        this.phone = newPhone;
    },
    getPhone: function(){
        if(this.phone.length == 0){
            return this.defaultPhone;
        }
        return this.phone;
    },
    setMobile: function(newMobile){
        this.mobile = newMobile;
    },
    getMobile: function(){
        if(this.mobile.length == 0){
            return this.defaultMobile;
        }
        return this.mobile;
    },
    generateHTML: function(){
        get_html_signature_full_visual = document.getElementById("signature-full-visual").innerHTML;
        $("#signature-full-html textarea").text(get_html_signature_full_visual);
    },
    render: function(){
        $("span.output_full_name").html(this.getName());
        $("span.output_company").html(this.getCompany());
        $(".image-preview").attr("src", this.getProfile());
        $("span.output_position").html(this.getPosition());
        $("span.output_email_address").html(this.getEmail());
        $("span.output_phone_office").html(this.getPhone());
        $("span.output_phone_mobile").html(this.getMobile());
        this.generateHTML();
    },
    reset: function(){
        document.getElementById("employee-information-form").reset();
        $(".input_image_url input").change();
        $(".input_full_name input").change();
        $(".input_company input").change();
        $(".input_position input").change();
        $(".input_email_address input").change();
        $(".input_phone_office input").change();
        $(".input_phone_mobile input").change();
    },
    functionalities: function(){
        elem = this;
        $(".input_image_url input").on("change keyup paste", function(){
            elem.setProfile($(this).val());
            $("span.output_full_name").html(elem.getProfile());
            elem.render();
        });
        $(".input_full_name input").on("change keyup paste", function(){
            elem.setName($(this).val());
            $("span.output_full_name").html(elem.getName());
            elem.render();
        });
        $(".input_company input").on("change keyup paste", function(){
            elem.setCompany($(this).val());
            $("span.output_company").html(elem.getCompany());
            elem.render();
        });
        $(".input_position input").on("change keyup paste", function(){
            elem.setPosition($(this).val());
            $("span.output_position").html(elem.getPosition());
            elem.render();
        });
        $(".input_email_address input").on("change keyup paste", function(){
            elem.setEmail($(this).val());
            $("span.output_email_address").html(elem.getEmail());
            elem.render();
        });
        $(".input_phone_office input").on("change keyup paste", function(){
            elem.setPhone($(this).val());
            $("span.output_phone_office").html(elem.getPhone());
            elem.render();
        });
        $(".input_phone_mobile input").on("change keyup paste", function(){
            elem.setMobile($(this).val());
            $("span.output_phone_mobile").html(elem.getMobile());
            elem.render();
        });
        $("#reset-form").click(function(e) {
            e.preventDefault();
            elem.reset();
        });
    },
    initialize: function(){
        this.render();
        this.functionalities();
    }
}

employee.initialize();
