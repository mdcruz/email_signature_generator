// Full name
// Check input field for data
$(".input_full_name input").on("change keyup paste", function(){
    var full_name = $(this).val();
    if(full_name) {
        $("span.output_full_name").html(full_name);
    }
    else {
        $("span.output_full_name").html(employee.full_name);
    }
    updateHtmlSigRaw()
});

//
// Company
// Check input field for data
$(".input_company input").on("change keyup paste", function(){
    var company = $(this).val();
    if(company) {
        $("span.output_company").html(company);
    }
    else {
        $("span.output_company").html(employee.company);
    }
    updateHtmlSigRaw()
});

//
// Position
// Check input field for data
$(".input_position input").on("change keyup paste", function(){
    var position = $(this).val();
    if(position) {
        $("span.output_position").html(position);
    }
    else {
        $("span.output_position").html(employee.position);
    }
    updateHtmlSigRaw()
});

//
// Email address
// Check input field for data
$(".input_email_address input").on("change keyup paste", function(){
    var email_address = $(this).val();
    if(email_address) {
        $("span.output_email_address").html(email_address);
        $(".email_address_anchor").attr("href", "mailto:"+email_address);
    }
    else {
        $("span.output_email_address").html(employee.email_address);
        $(".email_address_anchor").attr("href", "mailto:"+employee.email_address);
    }
    updateHtmlSigRaw()
});

// Hide optional phone numbers on DOM load
$(".phone_numbers_element").hide();
//
var phone_office = "";
var phone_mobile = "";
// Office phone number
// Field is optional. Hide on DOM load
$(".phone_office_element").hide();
// Check input field for data
$(".input_phone_office input").on("change keyup paste", function(){
    phone_office = $(this).val();
    if(phone_office) {
        $(".phone_numbers_element").show();
        $(".phone_office_element").show();
        $("span.output_phone_office").html(phone_office);
    }
    else {
        $(".phone_office_element").hide();
    }
    if(!phone_office && !phone_mobile) {
        $(".phone_numbers_element").hide();
    }
    updateHtmlSigRaw()
});
// Mobile phone number
// Field is optional. Hide on DOM load
$(".phone_mobile_element").hide();
// Check input field for data
$(".input_phone_mobile input").on("change keyup paste", function(){
    var phone_mobile = $(this).val();
    if(phone_mobile) {
        $(".phone_numbers_element").show();
        $(".phone_mobile_element").show();
        $("span.output_phone_mobile").html(phone_mobile);
    }
    else {
        $(".phone_mobile_element").hide();
    }
    if(!phone_office && !phone_mobile) {
        $(".phone_numbers_element").hide();
    }
    updateHtmlSigRaw()
});

//
// Twitter
// Field is optional. Hide on DOM load
$(".twitter_element").hide();
// Check input field for data
$(".input_twitter_username input").on("change keyup paste", function(){
    var twitter_username = $(this).val();
    if(twitter_username) {
        $(".twitter_element").show();
        $("span.output_twitter_username").html(twitter_username);
        $(".twitter_anchor").attr("href", "https://twitter.com/"+twitter_username);
    }
    else {
        $(".twitter_element").hide();
    }
    updateHtmlSigRaw()
});

////////////////////////
// Raw HTML version
////////////////////////
//
// Fill in HTML signature on DOM load
// Must be the last line of this file
updateHtmlSigRaw();
